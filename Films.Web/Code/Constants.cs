﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Films.Web
{
	public class Constants
	{
		public const string DateFormat = "MM/dd/yyyy";
        public const int PAGE_SIZE = 5;
	}
}
