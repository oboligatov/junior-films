﻿namespace Films.Web
{
    public enum AlertType
    {
        Error,
        Warning,
        Success,
        Empty
    }
}