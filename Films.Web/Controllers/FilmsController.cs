﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Films.BLL.Services;
using Films.DAL.Model;
using Films.Web.Models;
using System.IO;
using PagedList;

namespace Films.Web.Controllers
{
    [Authorize]
    public class FilmsController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ReturnToList()
        {
            TempData["PageIndexFromSession"] = true;
            return RedirectToAction("Index");
        }
                
        public ActionResult FilmsList(int pageIndex)
        {
            if (TempData["PageIndexFromSession"] != null)
                if (Session["FilmsList_PageIndex"] != null)
                {
                    pageIndex = Convert.ToInt32(Session["FilmsList_PageIndex"]);
                    TempData["PageIndexFromSession"] = null;
                }

            List<MovieListItemModel> model = new List<MovieListItemModel>();
            using (IMovieService service = new MovieService())
            {
                List<Movie> itemList = service.GetList().ToList();
                itemList.ForEach(x => model.Add(new MovieListItemModel(x)));
            }

            Session["FilmsList_PageIndex"] = pageIndex;

            return View(model.ToPagedList(pageIndex, Constants.PAGE_SIZE));
        }

        public ActionResult Edit(int? ID)
        {
            MovieEditModel model;
            if (ID.HasValue)
            {
                using (IMovieService service = new MovieService())
                {
                    model = new MovieEditModel(service.GetMovieById(ID.Value));
                    if (model.UserId != CurrentUserId)
                        return new HttpNotFoundResult("Not found!");
                }
            }
            else
                model = new MovieEditModel();

            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(MovieEditModel model, HttpPostedFileBase PosterFile)
        {
            if (!ModelState.IsValid)
            {
                ShowAlert(AlertType.Error, GetModelErrors());
                return View(model);
            }
            
            if (!model.IsEdit)
                if ((PosterFile == null) || (PosterFile.ContentLength == 0))
                {
                    ShowAlert(AlertType.Error, "Poster file is required");
                    return View(model);
                }

            using (IMovieService service = new MovieService())
            {
                Movie dbItem = new Movie();
                if (model.IsEdit)
                {
                    dbItem = service.GetMovieById(model.MovieId);
                }
                else
                {
                    dbItem.UserId = CurrentUserId;
                }

                dbItem.MovieName = model.MovieName;
                dbItem.Director = model.Director;
                dbItem.Description = model.Description;
                dbItem.MovieYear = model.MovieYear;

                if ((PosterFile != null) && (PosterFile.ContentLength != 0))
                    using (var binaryReader = new BinaryReader(PosterFile.InputStream))
                    {
                        dbItem.Poster = binaryReader.ReadBytes(PosterFile.ContentLength);
                    }
                
                if (model.MovieId > 0)
                    service.Update(dbItem);
                else
                    service.Add(dbItem);

            }

            return RedirectToAction("ReturnToList");

        }

        public ActionResult Delete(int id)
        {
            using (IMovieService service = new MovieService())
            {
                if (service.GetMovieById(id).UserId != CurrentUserId)
                    return new HttpNotFoundResult("Not found!");

                service.Delete(id);
            }
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Info(int ID)
        {
            MovieEditModel model;
            
                using (IMovieService service = new MovieService())
                {
                    Movie dbItem = service.GetMovieById(ID);
                    if (dbItem == null)
                        return new HttpNotFoundResult("Not found!");

                    model = new MovieEditModel(dbItem);
                }

            return View(model);
        }

    }
}
