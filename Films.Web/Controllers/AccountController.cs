﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Films.BLL.Services;
using Films.DAL.Model;
using Films.Web.Models;
using Films.Web.Security;

namespace Films.Web.Controllers
{
    public class AccountController : BaseController
    {
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                using (IUserService service = new UserService())
                {
                    if (service.Validate(model.UserName, model.Password))
                    {
                        FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                        return RedirectToLocal(returnUrl);
                    }

                    ShowAlert(AlertType.Error, "Incorrect Username or Password");
                    return View(model);
                }
            }

            ShowAlert(AlertType.Error, "Incorrect Username or Password");
            return View(model);
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegistrationModel model)
        {
            if (ModelState.IsValid)
            {
                using (IUserService service = new UserService())
                {
                    if (service.GetUserByName(model.UserLogin) != null)
                        ShowAlert(AlertType.Error, "User with such login already exists");
                    else
                    {
                        User dbItem = new User();
                        dbItem.UserLogin = model.UserLogin;
                        dbItem.UserName = model.UserName;
                        dbItem.Password = model.Password;

                        service.Add(dbItem);

                        FormsAuthentication.SetAuthCookie(model.UserLogin, false);
                        return RedirectToLocal("");
                    }
                }
            }

            return View(model);
        }
        
        public void LogOut()
        {
            if (UserPrincipal != null)
            {
                new CurrentUserService().DeletePrincipal(HttpContext);
            }
            
            FormsAuthentication.SignOut();
            Session.Abandon();

            // clear authentication cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);
            // clear session cookie (recommend)
            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);
            
            FormsAuthentication.RedirectToLoginPage();
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Films");
            }
        }

    }
}
