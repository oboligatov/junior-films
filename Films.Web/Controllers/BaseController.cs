﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Films.Web.Security;

namespace Films.Web.Controllers
{
    public class BaseController : Controller
    {
        protected string GetModelErrors()
        {
            var errors = (from modelState in ViewData.ModelState.Values
                          from error in modelState.Errors
                          select error.ErrorMessage).ToList();

            return string.Join("<br />", errors);
        }

        public void ShowAlert(AlertType type, string message)
        {
            TempData["AlertType"] = type;
            TempData["AlertMessage"] = message;
        }

        public void HideAlert()
        {
            TempData["AlertType"] = null;
            TempData["AlertMessage"] = null;
        }

        public UserPrincipal UserPrincipal
        {
            get { return UserPrincipal.Current; }
        }

        public int CurrentUserId
        {
            get
            {
                return UserPrincipal.UserId;
            }
        }
    }
}
