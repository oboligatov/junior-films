﻿using System;
using System.Web;
using System.Web.Caching;
using Films.BLL.Services;

namespace Films.Web.Security
{
    public interface ICurrentUserService
    {
        UserPrincipal GetPrincipal( HttpContextBase httpContext );
        void DeletePrincipal( HttpContextBase httpContext );
    }

    public class CurrentUserService : ICurrentUserService
    {
        public UserPrincipal GetPrincipal( HttpContextBase httpContext )
        {
            if ( !httpContext.User.Identity.IsAuthenticated )
            {
                return null;
            }
            var userName = httpContext.User.Identity.Name;
            var principal = (UserPrincipal)httpContext.Cache.Get( userName );
            if ( principal == null )
            {
                using ( var service = new UserService())
                {
                    var user = service.GetUserByName(userName);
                    if ( user == null )
                    {
                        return null;
                    }
                    principal = new UserPrincipal
                    {
                        Name = user.UserLogin,
                        DisplayName = user.UserName,
                        UserId = user.UserId,
                        IsAuthenticated = true
                    };
                    httpContext.Cache.Add( userName, principal, null, Cache.NoAbsoluteExpiration, new TimeSpan( 0, 30, 0 ), CacheItemPriority.Default, null );
                }
            }
            return principal;
        }

        public void DeletePrincipal( HttpContextBase httpContext )
        {
            if ( httpContext.User.Identity.IsAuthenticated )
            {
                httpContext.Cache.Remove( httpContext.User.Identity.Name );
            }
            httpContext.User = null;
        }
    }
}