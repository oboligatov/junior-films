﻿using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Films.Web.Security
{
    public class UserPrincipal : IPrincipal, IIdentity
    {
        public static UserPrincipal Current
        {
            get
            {
                var httpContext = HttpContext.Current;
                if ( httpContext == null )
                {
                    return null;
                }
                return httpContext.User as UserPrincipal;
            }
        }

        public IIdentity Identity { get { return this; } }

        public int UserId { get; set; }

        public string Name { get; set; }
        public string DisplayName { get; set; }
        
        public string AuthenticationType { get; set; }

        public bool IsAuthenticated { get; set; }

        public string[] Roles
        {
            get
            {
                return new string[] { "users" };
            }
        }

        public bool IsInRole(string role)
        {
            return Roles.Contains(role);
        }
    }
}