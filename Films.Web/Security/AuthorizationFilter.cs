﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Films.Web.Security
{
    public class AuthorizationFilter : IAuthorizationFilter
    {
        public void OnAuthorization( AuthorizationContext filterContext )
        {
            if (filterContext == null || filterContext.RequestContext == null)
            {
                return;
            }

            var contextUser = filterContext.HttpContext.User;
            if ( contextUser != null && contextUser.Identity.IsAuthenticated && UserPrincipal.Current == null )
            {
                var currentUserService = new CurrentUserService();
                filterContext.HttpContext.User = currentUserService.GetPrincipal(filterContext.HttpContext);
            }
        }
    }
}