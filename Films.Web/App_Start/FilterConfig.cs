﻿using System.Web;
using System.Web.Mvc;
using Films.Web.Security;

namespace Films.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters( GlobalFilterCollection filters )
        {
            filters.Add( new HandleErrorAttribute() );
            filters.Add( new AuthorizationFilter() );
        }
    }
}