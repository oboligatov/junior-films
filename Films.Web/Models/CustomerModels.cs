﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using Films.DAL.Model;

namespace Films.Web.Models
{
    public class CustomerEditModel
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        [MaxLength(500, ErrorMessage = "Max length for Name is 500 symbols")]
        public string CustomerName { get; set; }

        [Display(Name = "Address")]
        [MaxLength(500, ErrorMessage = "Max length for Address is 500 symbols")]
        public string Address { get; set; }

        public int CustomerId { get; set; }
        

        public CustomerEditModel()
        {
            CustomerId = -1;
        }

        public CustomerEditModel(Customer dbEntity)
        {
            if (dbEntity != null)
            {
                CustomerId = dbEntity.Id;
                CustomerName = dbEntity.Name;
                Address = dbEntity.Adress;
            }
        }

    }

}
