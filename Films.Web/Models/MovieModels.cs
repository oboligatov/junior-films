﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using Films.Web;
using Films.DAL.Model;

namespace Films.Web.Models
{
    public class MovieEditModel
    {
        public int MovieId { get; set; }

        [Display(Name = "Author")]
        public string UserName { get; set; }
        public int UserId { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        [MaxLength(500, ErrorMessage = "Max length for Name is 500 symbols")]
        public string MovieName { get; set; }

        [Display(Name = "Poster")]
        public byte[] Poster { get; set; }

        public string PosterUrl
        {
            get
            {
                if (Poster != null)
                {
                    string base64String = Convert.ToBase64String(Poster, 0, Poster.Length);
                    return "data:image/png;base64," + base64String;
                }
                else
                    return "";
            }
        }

        [Display(Name = "Year")]
        public int MovieYear { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }

        [Display(Name = "Director")]
        [Required(ErrorMessage = "Director is required")]
        [MaxLength(100, ErrorMessage = "Max length for Director is 100 symbols")]
        public string Director { get; set; }

        public List<int> YearList
        {
            get
            {
                List<int> result = new List<int>();
                for (int i = DateTime.Now.Year + 1; i >= 1895; --i)
                    result.Add(i);
                return result;
            }
        }

        public SelectList YearDDList
        {
            get
            {
                List<SelectListItem> result = new List<SelectListItem>();
                YearList.ForEach(x => result.Add(new SelectListItem() { Text = x.ToString(), Value = x.ToString(), Selected = (x == MovieYear) }));
                return new SelectList(result, "Value", "Text");
            }
        }

        public bool IsEdit
        {
            get
            {
                return MovieId > 0;
            }
        }
        

        public MovieEditModel()
        {
            MovieId = -1;
            MovieYear = DateTime.Now.Year;
        }

        public MovieEditModel(Movie dbEntity)
        {
            if (dbEntity != null)
            {
                MovieId = dbEntity.MovieId;
                MovieName = dbEntity.MovieName;
                Poster = dbEntity.Poster;
                MovieYear = dbEntity.MovieYear;
                Description = dbEntity.Description;
                Director = dbEntity.Director;
                UserName = dbEntity.User.UserName;
                UserId = dbEntity.UserId;
            }
        }
    }

    public class MovieListItemModel
    {
        public int MovieId { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public string MovieName { get; set; }
        public byte[] Poster { get; set; }
        public string PosterUrl
        {
            get
            {
                string base64String = Convert.ToBase64String(Poster, 0, Poster.Length);
                return "data:image/png;base64," + base64String;
            }
        }
        public int MovieYear { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }

        public MovieListItemModel()
        {
        }

        public MovieListItemModel(Movie dbEntity)
        {
            if (dbEntity != null)
            {
                MovieId = dbEntity.MovieId;
                MovieName = dbEntity.MovieName;
                Poster = dbEntity.Poster;
                MovieYear = dbEntity.MovieYear;
                Description = dbEntity.Description;
                Director = dbEntity.Director;
                UserName = dbEntity.User.UserName;
                UserId = dbEntity.UserId;
            }
        }
    }
}
