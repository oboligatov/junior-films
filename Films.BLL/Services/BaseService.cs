﻿using System;
using Films.DAL.Repository;

namespace Films.BLL.Services
{
    public abstract class BaseService<TRepository> where TRepository : class, IRepository, new()
    {
        private TRepository repository;
        
        public TRepository Repository
        {
            get { return repository ?? (repository = new TRepository()); }
        } 
    }
}
