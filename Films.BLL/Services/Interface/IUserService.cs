﻿using System;
using System.Collections.Generic;
using Films.DAL.Model;

namespace Films.BLL.Services
{
    public interface IUserService :  IBaseService<User>, IDisposable
    {
        bool Validate(string username, string password);
        User GetUserById(int id);
        User GetUserByName(string username);
        void Add(User dbItem);
    }
}
