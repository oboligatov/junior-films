﻿using System;
using System.Collections.Generic;
using Films.DAL.Model;

namespace Films.BLL.Services
{
    public interface IMovieService :  IBaseService<Movie>, IDisposable
    {
        IEnumerable<Movie> GetList();
        Movie GetMovieById(int movieId);

        void Add(Movie dbItem);
        void Update(Movie dbItem);
        void Delete(int movieId);
    }
}
