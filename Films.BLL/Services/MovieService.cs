﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Films.DAL.Repository;
using Films.DAL.Model;

namespace Films.BLL.Services
{
    public class MovieService : BaseService<MovieRepository>, IMovieService
    {
        public void Dispose()
        {
            Repository.Dispose();
        }

        public IEnumerable<Movie> GetList()
        {
            return Repository.GetAll().OrderByDescending(x => x.MovieId);
        }

        public Movie GetMovieById(int movieId)
        {
            return Repository.GetAll().FirstOrDefault(x => x.MovieId == movieId);
        }

        public void Add(Movie dbItem)
        {
            Repository.Add(dbItem);
        }

        public void Update(Movie dbItem)
        {
            Repository.Update(dbItem);
        }

        public void Delete(int movieId)
        {
            Movie dbItem = Repository.Get(movieId);
            if (dbItem != null)
                Repository.Delete(dbItem);
        }
    }
}
