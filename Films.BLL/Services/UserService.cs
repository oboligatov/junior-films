﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Films.DAL.Repository;
using Films.DAL.Model;

namespace Films.BLL.Services
{
    public class UserService : BaseService<UserRepository>, IUserService
    {
        public void Dispose()
        {
            Repository.Dispose();
        }

        public bool Validate(string username, string password)
        {
            return Repository.GetAll().Any(x => x.UserLogin == username && x.Password == password);
        }

        public User GetUserById(int id)
        {
            return Repository.GetAll().FirstOrDefault(x => x.UserId == id);
        }

        public User GetUserByName(string username)
        {
            return Repository.GetAll().FirstOrDefault(x => x.UserLogin == username);
        }

        public void Add(User dbItem)
        {
            Repository.Add(dbItem);
        }

    }
}
