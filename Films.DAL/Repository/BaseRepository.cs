﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using EntityState = System.Data.Entity.EntityState;

namespace Films.DAL.Repository
{
    public abstract class BaseRepository<T> : DatabaseContext, IRepository<T> where T : class, new()
    {
        public T Add(T item)
        {
            DbContext.Set<T>().Add(item);
            DbContext.SaveChanges();

            return item;
        }

        public T Get(object id)
        {
            return DbContext.Set<T>().Find(id);
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().AsQueryable().FirstOrDefault(predicate);
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate).Count();
        }

        public IQueryable<T> GetCollection(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return DbContext.Set<T>().AsQueryable();
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().AsQueryable().Where(predicate);
        }

        public void Update(T item)
        {
            DbContext.Entry(item).State = (EntityState)EntityState.Modified;
            DbContext.SaveChanges();
        }

        public void Delete(T item)
        {
            DbContext.Set<T>().Remove(item);
            DbContext.SaveChanges();
        }

        public void DeleteRange(IEnumerable<T> items)
        {
            DbContext.Set<T>().RemoveRange(items);
            DbContext.SaveChanges();
        }
    }
}
