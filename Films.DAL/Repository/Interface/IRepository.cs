﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Films.DAL.Repository
{
    public interface IRepository
    {
    }

    public interface IRepository<T> : IRepository
    where T : class, new()
    {
        T Add(T item);

        T Get(object id);

        T Get(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll();

        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);

        void Update(T item);

        void Delete(T item);

        void DeleteRange(IEnumerable<T> items);
    }
}
