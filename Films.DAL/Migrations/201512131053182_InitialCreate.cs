namespace Films.DAL
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Movie",
                c => new
                    {
                        MovieId = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        MovieName = c.String(nullable: false, maxLength: 500),
                        Poster = c.Binary(),
                        MovieYear = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        Director = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.MovieId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 256),
                        UserLogin = c.String(nullable: false, maxLength: 256),
                        Password = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Movie", "UserId", "dbo.Users");
            DropIndex("dbo.Movie", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.Movie");
        }
    }
}
