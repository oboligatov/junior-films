﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Films.DAL.Model;

namespace Films.DAL
{
    public interface IDbContext : IDisposable
    {
        DbSet<T> Set<T>() where T : class;
        DbEntityEntry<T> Entry<T>( T entity ) where T : class;
        int SaveChanges();
        void SetModified(object entity);

        DbSet<User> User { get; set; }
        DbSet<Movie> Movie { get; set; }
    }
}
