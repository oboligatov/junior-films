﻿using System;

namespace Films.DAL
{
    public abstract class DatabaseContext : IDisposable
    {
        private IDbContext _contextDb;

        public IDbContext DbContext
        {
            get { return _contextDb ?? ( _contextDb = new MyContext() ); }
            set { _contextDb = value; }
        }

        public void Dispose()
        {
            if ( _contextDb != null )
            {
                _contextDb.Dispose();
                _contextDb = null;
            }

            GC.SuppressFinalize(this);
        }
    }
}
