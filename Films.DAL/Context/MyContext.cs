using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity.Migrations;
using Films.DAL.Model;

namespace Films.DAL
{
    public partial class MyContext : DbContext, IDbContext
    {
        private const string ConnectionString = "Name=AppConnectionString";

        public MyContext()
            : base(ConnectionString)
        {
            Database.SetInitializer<MyContext>(new MigrateDatabaseToLatestVersion<MyContext, Configuration>());
        }

        public MyContext(bool autoDetectChanges, bool lazyLoading)
            : base(ConnectionString)
        {
            Configuration.AutoDetectChangesEnabled = autoDetectChanges;
            Configuration.LazyLoadingEnabled = lazyLoading;
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Movie> Movie { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
        }
    }

    public sealed class Configuration : DbMigrationsConfiguration<MyContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MyContext context)
        {

        }
    }
}
