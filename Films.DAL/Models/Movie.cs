using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Films.DAL.Model
{
    [Table("Movie")]
    public partial class Movie
    {
        public Movie()
        {
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int MovieId { get; set; }

        public int UserId { get; set; }

        [Required]
        [StringLength(500)]
        public string MovieName { get; set; }

        public byte[] Poster { get; set; }

        public int MovieYear { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        [StringLength(100)]
        public string Director { get; set; }
        
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
