using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Films.DAL.Model
{
    [Table("Users")]
    public partial class User
    {
        public User()
        {
            Movie = new HashSet<Movie>();
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        [Required]
        [StringLength(256)]
        public string UserName { get; set; }

        [Required]
        [StringLength(256)]
        public string UserLogin { get; set; }

        [Required]
        [StringLength(128)]
        public string Password { get; set; }

        public virtual ICollection<Movie> Movie { get; set; }
    }
}
